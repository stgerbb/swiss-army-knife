package ch.ffhs.esa.swissarmyknife.manager;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import static org.junit.Assert.assertEquals;

/**
 * FileHandler test class
 * Tests if the methods writeToFile and readFromFile work correctly.
 *
 * @version 1.0
 */
public class FileHandlerTest {

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void testReadFromEmptyFile() throws Exception {
        FileHandler fileHandler = new FileHandler();
        File testFile = tempFolder.newFile("testFile.txt");

        FileInputStream input = new FileInputStream(testFile);
        assertEquals(fileHandler.readFromFile(input), "");
    }

    @Test
    public void testWriteAndReadAFile() throws Exception {

        FileHandler fileHandler = new FileHandler();
        File testFile = tempFolder.newFile("testFile.txt");
        String testString = "123456789";

        FileOutputStream output = new FileOutputStream(testFile);
        fileHandler.writeToFile(output, testString);
        FileInputStream input = new FileInputStream(testFile);

        String string = fileHandler.readFromFile(input).trim();

        assertEquals(string, "123456789");
    }
}