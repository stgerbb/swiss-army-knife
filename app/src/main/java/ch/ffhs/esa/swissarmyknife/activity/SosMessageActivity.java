package ch.ffhs.esa.swissarmyknife.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ch.ffhs.esa.swissarmyknife.R;
import ch.ffhs.esa.swissarmyknife.manager.FileHandler;

/**
 * SosMessage UI
 * Activity to create, save and delete a SOS message. Uses a text file to persistently save a message.
 *
 * @version 1.0
 */
public class SosMessageActivity extends AppCompatActivity {
    private static final String FILE_NAME = "emergency_message.txt";
    private static final int MIN_MESSAGE_LENGTH = 17;
    private EditText txtSosMessage;
    private FileHandler fileHandler;
    private View activity_sos_message;

    public void onClickSaveMessage(View v) {
        this.saveToFile();
    }

    private void saveToFile() {
        if (this.txtSosMessage.length() > MIN_MESSAGE_LENGTH) {
            String message = this.txtSosMessage.getText().toString();
            FileOutputStream file = null;
            try {
                file = openFileOutput(FILE_NAME, MODE_PRIVATE);
                this.fileHandler.writeToFile(file, message);
            } catch(FileNotFoundException e) {
                Snackbar.make(this.activity_sos_message, R.string.file_not_found, Snackbar.LENGTH_LONG).show();
            } finally {
                try {
                    file.close();
                    Snackbar.make(this.activity_sos_message, R.string.message_saving_ok, Snackbar.LENGTH_LONG).show();
                } catch(IOException e2) {
                    Snackbar.make(this.activity_sos_message, R.string.message_saving_error, Snackbar.LENGTH_LONG).show();
                }
            }
        } else {
            Snackbar.make(this.activity_sos_message, R.string.message_to_short, Snackbar.LENGTH_LONG).show();
        }
    }

    public void onClickEraseMessage(View v) {
        this.txtSosMessage.getText().clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos_message);
        this.txtSosMessage = findViewById(R.id.txt_sos_message);
        this.fileHandler = new FileHandler();
        this.activity_sos_message = findViewById(R.id.activity_sos_message);
        this.txtSosMessage.setText(this.loadFromFile());
    }

    private String loadFromFile() {
        FileInputStream stream = null;
        try {
            stream = openFileInput(FILE_NAME);
            String content = this.fileHandler.readFromFile(stream);
            return content;
        } catch(FileNotFoundException e) {
            Snackbar.make(this.activity_sos_message, R.string.file_not_found, Snackbar.LENGTH_LONG).show();
            return getResources().getString(R.string.txt_default_sos_message);
        } finally {
            try {
                if(stream != null) {
                    stream.close();
                }
            } catch(IOException e2) {
                Snackbar.make(this.activity_sos_message, R.string.stream_error, Snackbar.LENGTH_LONG).show();
            }
        }
    }
}