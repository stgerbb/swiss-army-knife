package ch.ffhs.esa.swissarmyknife.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

/**
 * Locator class
 * This is a service Class that implements the LocationListener. With this class the actual location
 * of the user can be determined.
 *
 * @version 1.0
 */
public class Locator extends Service implements LocationListener {
    private IBinder binder;
    private LocationManager locationManager;
    private Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        this.binder = new LocalBinder();
        this.locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        this.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5, 10, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.locationManager.removeUpdates(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return this.binder;
    }

    @Override
    public void onLocationChanged(Location location) {
        Bundle bundle = new Bundle();
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        bundle.putDouble("latitude", latitude);
        bundle.putDouble("longitude", longitude);
        Message message = new Message();
        message.setData(bundle);
        this.handler.sendMessage(message);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        // Not in use
    }

    @Override
    public void onProviderEnabled(String s) {
        // Not in use
    }

    @Override
    public void onProviderDisabled(String s) {
        // Not in use
    }

    public class LocalBinder extends Binder {
        public Locator getService() {
            return Locator.this;
        }
    }
}