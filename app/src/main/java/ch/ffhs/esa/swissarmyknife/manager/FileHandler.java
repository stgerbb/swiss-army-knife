package ch.ffhs.esa.swissarmyknife.manager;

import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * FileHandler class
 * This class is used to handle file in- and output. It provides methods to write text to a file and
 * read text from a file.
 *
 * @version 1.0
 */
public class FileHandler {
    private static final String TAG = "FileHandler";

    public void writeToFile(FileOutputStream stream, String message) {
        OutputStreamWriter writer = new OutputStreamWriter(stream);
        try {
            writer.write(message);
            writer.flush();
        } catch(IOException e) {
            Log.i(TAG, "IOException: " + e);
        } finally {
            try {
                writer.close();
            } catch(IOException e2) {
                Log.i(TAG, "IOException: " + e2);
            }
        }
    }

    public String readFromFile(FileInputStream stream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder content = new StringBuilder();
        String row = "";
        try {
            while((row = bufferedReader.readLine()) != null) {
                content.append(row + "\n");
            }
            return content.toString();
        } catch(IOException e) {
            Log.i(TAG, "IOException: " + e);
            return null;
        } finally {
            try {
                bufferedReader.close();
            } catch(IOException e2) {
                Log.i(TAG, "IOException: " + e2);
            }
        }
    }
}