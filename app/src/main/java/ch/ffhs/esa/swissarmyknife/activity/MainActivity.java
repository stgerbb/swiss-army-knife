package ch.ffhs.esa.swissarmyknife.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import ch.ffhs.esa.swissarmyknife.R;

/**
 * Main UI
 * Every functionality of the app can be called through this UI.
 *
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {
    private View activity_main;
    private CameraManager cameraManager;
    private String cameraId;
    private boolean isFlashOn;
    private Button btnFlashlight;

    public void onClickOpenActivityLocation(View v) {
        Intent intent = new Intent(this, LocationActivity.class);
        startActivity(intent);
    }

    public void onClickOpenActivityLoudness(View v) {
        Intent intent = new Intent(this, LoudnessActivity.class);
        startActivity(intent);
    }

    public void onClickFlashlight(View v) {
        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) && Build.VERSION.SDK_INT >= 23) {
            if(this.isFlashOn) {
                this.turnFlashOff();
            } else {
                this.turnFlashOn();;
            }
        } else {
            Snackbar.make(this.activity_main, R.string.no_flash, Snackbar.LENGTH_LONG).show();
        }
    }

    public void onClickOpenActivityCompass(View v) {
        Intent intent = new Intent(this, CompassActivity.class);
        startActivity(intent);
    }

    public void onClickOpenActivitySpiritLevel(View v) {
        Intent intent = new Intent(this, SpiritLevelActivity.class);
        startActivity(intent);
    }

    public void onClickOpenActivityHeightMeter(View v) {
        Intent intent = new Intent(this, HeightMeterActivity.class);
        startActivity(intent);
    }

    public void onClickOpenActivitySendSos(View v) {
        Intent intent = new Intent(this, SendSosActivity.class);
        startActivity(intent);
    }

    @SuppressLint("NewApi")
    private void turnFlashOff() {
        try {
            this.cameraManager.setTorchMode(this.cameraId, false);
        } catch(CameraAccessException e) {
            Snackbar.make(this.activity_main, e.getStackTrace().toString(), Snackbar.LENGTH_LONG).show();
        } finally {
            this.isFlashOn = false;
            this.btnFlashlight.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        }
    }

    @SuppressLint("NewApi")
    private void turnFlashOn() {
        try {
            this.cameraManager.setTorchMode(this.cameraId, true);
            this.isFlashOn = true;
            this.btnFlashlight.setBackgroundColor(getResources().getColor(R.color.colorYellow));
        } catch (CameraAccessException e) {
            Snackbar.make(this.activity_main, e.getStackTrace().toString(), Snackbar.LENGTH_LONG).show();
            this.isFlashOn = false;
        }
    }

    private void initFlash() {
        this.activity_main = findViewById(R.id.activity_main_layout);
        this.btnFlashlight = findViewById(R.id.btn_flashlight);
        this.cameraManager = (CameraManager) getSystemService(CAMERA_SERVICE);
        try {
            this.cameraId = this.cameraManager.getCameraIdList()[0];
        } catch(CameraAccessException e) {
            Snackbar.make(this.activity_main, R.string.no_camera, Snackbar.LENGTH_LONG).show();
        }
        this.isFlashOn = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        this.initFlash();
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            this.onOrientationLandscape();
        }
    }

    private void onOrientationLandscape() {
        Button btnLocation = findViewById(R.id.btn_location);
        Button btnLoudness = findViewById(R.id.btn_loudness);
        Button btnFlashlight = findViewById(R.id.btn_flashlight);
        Button btnCompass = findViewById(R.id.btn_compass);
        Button btnSpiritLevel = findViewById(R.id.btn_spirit_level);
        Button btnHeightMeter = findViewById(R.id.btn_height_meter);

        btnLocation.setPadding(0, 0, 0, 0);
        btnLocation.setCompoundDrawables(null, null, null, null);
        btnLoudness.setPadding(0, 0, 0, 0);
        btnLoudness.setCompoundDrawables(null, null, null, null);
        btnFlashlight.setPadding(0, 0, 0, 0);
        btnFlashlight.setCompoundDrawables(null, null, null, null);
        btnCompass.setPadding(0, 0, 0, 0);
        btnCompass.setCompoundDrawables(null, null, null, null);
        btnSpiritLevel.setPadding(0, 0, 0, 0);
        btnSpiritLevel.setCompoundDrawables(null, null, null, null);
        btnHeightMeter.setPadding(0, 0, 0, 0);
        btnHeightMeter.setCompoundDrawables(null, null, null, null);
    }

    private boolean getSharedPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences("flashlight", MODE_PRIVATE);
        return sharedPreferences.getBoolean("flash", false);
    }

    private void saveToSharedPreferences(String key, boolean item) {
        SharedPreferences sharedPreferences = getSharedPreferences("flashlight", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, item);
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean flash = this.getSharedPreferences();
        if(flash) {
            this.turnFlashOn();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.saveToSharedPreferences("flash", this.isFlashOn);
        if (this.isFlashOn) {
            this.turnFlashOff();
        }
    }
}