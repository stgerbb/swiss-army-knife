package ch.ffhs.esa.swissarmyknife.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ch.ffhs.esa.swissarmyknife.R;

/**
 *
 * Contact UI
 * Subactivity to show and select stored contacts on the Smartphone in a ListView.
 *
 * @version 1.0
 */
public class ContactActivity extends ListActivity {

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        Object item = listView.getItemAtPosition(position);
        String[] contact = item.toString().split("=");
        Intent intent = new Intent(this, ContactActivity.class);
        intent.putExtra("contact", contact);
        setResult(10, intent);
        finish();
    }

    /**
     * Queries saved contacts on the smartphone
     * @return HashMap<String, String>
     */
    public HashMap<String, String> queryContacts() {
        HashMap<String, String> map = new HashMap<String, String>();
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        if(cursor.getCount() > 0) {
            while(cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String mobile = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                map.put(name, mobile);
            }
            cursor.close();
        }
        return map;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        HashMap<String, String> map = queryContacts();
        MyAdapter adapter = new MyAdapter(map);
        setListAdapter(adapter);
    }

    /**
     * Defines a custom adapter to save a HashMap<String, String> to ListView
     */
    public class MyAdapter extends BaseAdapter {
        private ArrayList data;

        public MyAdapter(HashMap<String, String> map) {
            this.data = new ArrayList();
            this.data.addAll(map.entrySet());
        }

        @Override
        public int getCount() {
            return this.data.size();
        }

        @Override
        public Map.Entry<String, String> getItem(int position) {
            return (Map.Entry) this.data.get(position);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            View result;
            if(view == null) {
                result = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hashmap_listview, viewGroup, false);
            } else {
                result = view;
            }
            Map.Entry<String, String> item = getItem(position);
            ((TextView) result.findViewById(R.id.text1)).setText(item.getKey());
            ((TextView) result.findViewById(R.id.text2)).setText(item.getValue());
            return result;
        }
    }
}