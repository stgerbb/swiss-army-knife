package ch.ffhs.esa.swissarmyknife.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import ch.ffhs.esa.swissarmyknife.R;

import static java.lang.Math.exp;

/**
 * Loudness UI
 * This Activity Class is responsible for the Loudness Measurements functionality. With the microphone of the phone, the actual loudness
 * is going to be calculated and showed to the screen.
 *
 * @version 1.0
 */
public class LoudnessActivity extends AppCompatActivity {
    private static final double EMA_FILTER = 0.6;
    private double mEMA;
    private MediaRecorder recorder;
    private View activity_loudness;
    private TextView loudnessView;
    private boolean started;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loudness);
        this.activity_loudness = findViewById(R.id.activity_loudness);
        this.recorder = null;
        this.started = false;
        this.mEMA = 0.0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 9) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.startRecorder();
            }
        }
    }

    private void checkPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.RECORD_AUDIO } , 10);
        } else {
            this.startRecorder();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
            this.checkPermission();
        } else {
            Snackbar.make(this.activity_loudness, R.string.txt_error_no_microphone, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(this.recorder != null) {
            this.stopRecorder();
        }
    }

    public void onClickMeasureLoudness(View v) {
        Button btn = (Button) v;
        if (this.started == false) {
            this.started = true;
            new LoudnessTask().execute("");
            btn.setText(R.string.imgbtn_stop_loudness_measurement);
        } else {
            this.started = false;
            btn.setText(R.string.imgbtn_measure_loudness);
        }
    }

    private void startRecorder() {
        this.recorder = new MediaRecorder();
        this.recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        this.recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        this.recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        this.recorder.setOutputFile("/dev/null");
        try {
            this.recorder.prepare();
        } catch (IOException e) {
            Snackbar.make(this.activity_loudness, R.string.io_exception + android.util.Log.getStackTraceString(e), Snackbar.LENGTH_LONG).show();
        } catch (SecurityException ex) {
            Snackbar.make(this.activity_loudness, R.string.security_exception + android.util.Log.getStackTraceString(ex), Snackbar.LENGTH_LONG).show();
        }
        try {
            this.recorder.start();
        } catch (SecurityException e) {
            Snackbar.make(this.activity_loudness, R.string.security_exception + android.util.Log.getStackTraceString(e), Snackbar.LENGTH_LONG).show();
        }
    }

    private void stopRecorder() {
        this.recorder.stop();
        this.recorder.release();
        this.recorder = null;
        this.started = false;
    }

    private double getAmplitude() {
        return this.recorder.getMaxAmplitude();
    }

    private double soundDb(double ampl) {
        return 20 * Math.log10(getAmplitudeEMA() / ampl);
    }

    private double getAmplitudeEMA() {
        double amp = getAmplitude();
        this.mEMA = this.EMA_FILTER * amp + (1.0 - this.EMA_FILTER) * this.mEMA;
        return this.mEMA;
    }

    /**
     * Task that updates the dbValue
     */
    private class LoudnessTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            loudnessView = findViewById(R.id.txt_loudness);
        }

        @Override
        protected String doInBackground(String... params) {
            while (started) {
                /*
                This benchmark value (ampVal) is key to correct decibel transformations.
                [-2] gives the best dbValue compared to an other sound meter app.
                */
                double ampVal = 10 * exp(-2);
                double dbValue = soundDb(ampVal);
                int dbValueInt = (int) dbValue;
                publishProgress(dbValueInt);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return "Stopped";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int localDbValue = values[0];
            loudnessView.setText(getApplicationContext().getString(R.string.txt_loudness_decibel, Integer.toString(localDbValue)));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }
}