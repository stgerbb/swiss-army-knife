package ch.ffhs.esa.swissarmyknife.activity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import ch.ffhs.esa.swissarmyknife.R;

/**
 * Compass UI
 * This UI shows the directions (N, E, S, W) to the user.
 *
 * @version 1.0
 */
public class
CompassActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private ImageView imageView;
    private float[] GravityArr;
    private float[] GeomagneticArr;
    private float azimuth;
    private float currentAzimuth;
    private View activity_compass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);
        this.sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.imageView = findViewById(R.id.imgCompass);
        this.activity_compass = findViewById(R.id.activity_compass_layout);
        this.GravityArr = new float[3];
        this.GeomagneticArr = new float[3];
        this.azimuth = 0f;
        this.currentAzimuth = 0f;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float alpha = 0.97f;
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            this.GravityArr[0] = alpha * GravityArr[0] + (1 - alpha) * sensorEvent.values[0];
            this.GravityArr[1] = alpha * GravityArr[1] + (1 - alpha) * sensorEvent.values[1];
            this.GravityArr[2] = alpha * GravityArr[2] + (1 - alpha) * sensorEvent.values[2];
        }
        if(sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            this.GeomagneticArr[0] = alpha * GeomagneticArr[0] + (1 - alpha) * sensorEvent.values[0];
            this.GeomagneticArr[1] = alpha * GeomagneticArr[1] + (1 - alpha) * sensorEvent.values[1];
            this.GeomagneticArr[2] = alpha * GeomagneticArr[2] + (1 - alpha) * sensorEvent.values[2];
        }
        float[] rotationMatrix = new float[9];
        float[] inclinationMatrix = new float[9];
        boolean success = SensorManager.getRotationMatrix(rotationMatrix, inclinationMatrix, this.GravityArr, this.GeomagneticArr);
        if(success) {
            float[] orientation = new float[3];
            SensorManager.getOrientation(rotationMatrix, orientation);
            this.azimuth = (float) Math.toDegrees(orientation[0]);
            this.azimuth = (azimuth + 360) % 360;
            Animation anim = new RotateAnimation(-this.currentAzimuth, this.azimuth, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            this.currentAzimuth = this.azimuth;
            anim.setDuration(500);
            anim.setRepeatCount(0);
            anim.setFillAfter(true);
            this.imageView.startAnimation(anim);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // Not in use
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null && this.sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
            this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), this.sensorManager.SENSOR_DELAY_NORMAL);
            this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), this.sensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Snackbar.make(this.activity_compass, R.string.no_accelerometer_magetic_field_sensor, Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(this.sensorManager != null) {
            this.sensorManager.unregisterListener(this);
        }
    }
}