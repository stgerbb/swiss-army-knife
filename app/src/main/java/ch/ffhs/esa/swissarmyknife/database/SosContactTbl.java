package ch.ffhs.esa.swissarmyknife.database;

/**
 * SosContactTbl class
 * This class implements the interface SosContactColumns and is a representation of the sos contact table.
 *
 * @version 2.0
 */
public final class SosContactTbl implements SosContactColumns {
    public static final String TABLE_NAME = "sos_contact";
    public static final String SQL_CREATE =
            "CREATE TABLE sos_contact (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "name VARCHAR(128) NOT NULL," +
                    "mobile_number VARCHAR(128) NOT NULL);";
}