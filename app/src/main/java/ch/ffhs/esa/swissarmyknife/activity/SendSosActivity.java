package ch.ffhs.esa.swissarmyknife.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import ch.ffhs.esa.swissarmyknife.R;
import ch.ffhs.esa.swissarmyknife.database.Database;
import ch.ffhs.esa.swissarmyknife.database.SosContactTbl;
import ch.ffhs.esa.swissarmyknife.manager.FileHandler;
import ch.ffhs.esa.swissarmyknife.service.Locator;

/**
 * SendSos UI
 * On this activity the user can send a sos message to a selected sos contact. From this activity the user can navigate
 * to the SosContactActivity and the SosMessageActivity.
 *
 * @version 1.0
 */
public class SendSosActivity extends AppCompatActivity implements ServiceConnection {
    private static final String FILE_NAME = "emergency_message.txt";
    private View activity_send_sos;
    private double latitude;
    private double longitude;
    private boolean hasMessage;
    private boolean isBound;

    private void bindService() {
        Intent intent = new Intent(this, Locator.class);
        bindService(intent, this, BIND_AUTO_CREATE);
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message message) {
            Bundle bundle = message.getData();
            latitude = bundle.getDouble("latitude");
            longitude = bundle.getDouble("longitude");
            hasMessage = true;
        }
    };

    private String getMessage() {
        FileInputStream stream = null;
        try {
            stream = openFileInput(FILE_NAME);
            FileHandler fileHandler = new FileHandler();
            String content = fileHandler.readFromFile(stream);
            return content;
        } catch(FileNotFoundException e) {
            Snackbar.make(this.activity_send_sos, R.string.file_not_found, Snackbar.LENGTH_LONG).show();
            return null;
        } finally {
            try {
                if(stream != null) {
                    stream.close();
                }
            } catch(IOException e2) {
                Snackbar.make(this.activity_send_sos, R.string.stream_error, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private long getSharedPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences("Number", MODE_PRIVATE);
        long itemId = sharedPreferences.getLong("itemId", 0);
        return itemId;
    }

    private String[] getContact() {
        String itemId = Long.toString(getSharedPreferences());
        Database db = new Database(this);
        SQLiteDatabase dbConn = db.getReadableDatabase();
        String[] dbColumns = new String[] { SosContactTbl.ID, SosContactTbl.NAME, SosContactTbl.MOBILE_NUMBER };
        Cursor cursor = dbConn.query(SosContactTbl.TABLE_NAME, dbColumns, "_id = ?" , new String[] { itemId }, null, null, null, null);
        if(cursor.moveToFirst()) {
            String number = cursor.getString(cursor.getColumnIndex(SosContactTbl.MOBILE_NUMBER));
            String name = cursor.getString(cursor.getColumnIndex(SosContactTbl.NAME));
            return new String[] { number, name };
        } else {
            return new String[] { null, null };
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 10) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.bindService();
            }
        } else if (requestCode == 20) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.sendSms();
            }
        }
    }

    private void sendSms() {
        String message = this.getMessage();
        String[] contact = this.getContact();
        String number = contact[0];
        String name = contact[1];
        if(message == null) {
            Snackbar.make(this.activity_send_sos, R.string.no_message, Snackbar.LENGTH_LONG).show();
        } else if(number == null) {
            Snackbar.make(this.activity_send_sos, R.string.no_number, Snackbar.LENGTH_LONG).show();
        } else if (this.hasMessage == false) {
            Snackbar.make(activity_send_sos, R.string.no_coordinates, Snackbar.LENGTH_LONG).show();
        } else {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.SEND_SMS }, 20);
            } else {
                String uri = "http://maps.google.com/maps?saddr=" + this.latitude + "," + this.longitude;
                StringBuilder smsMessage = new StringBuilder();
                smsMessage.append(message);
                smsMessage.append("Meine aktuelle Position auf Google Maps einsehen: " + Uri.parse(uri));
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(number, null, smsMessage.toString(), null, null);
                Snackbar.make(this.activity_send_sos, "Notruf wurde an " + name + " versandt", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    public void onClickOpenActivitySosMessage(View v) {
        Intent intent = new Intent(this, SosMessageActivity.class);
        startActivity(intent);
    }

    public void onClickOpenActivitySosContact(View v) {
        Intent intent = new Intent(this, SosContactActivity.class);
        startActivity(intent);
    }

    public void onClickSendSos(View v) {
        this.sendSms();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sos);
        this.activity_send_sos = findViewById(R.id.activity_send_sos);
        this.hasMessage = false;
        this.isBound = false;
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            this.onOrientationLandscape();
        }
    }

    private void onOrientationLandscape() {
        Button btnSosMessage = findViewById(R.id.btn_sos_message);
        Button btnSosContact = findViewById(R.id.btn_sos_contact);
        btnSosMessage.setPadding(0, 0, 0, 0);
        btnSosMessage.setCompoundDrawables(null, null, null, null);
        btnSosContact.setPadding(0, 0, 0, 0);
        btnSosContact.setCompoundDrawables(null, null, null, null);
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Locator.LocalBinder localBinder = (Locator.LocalBinder) iBinder;
        Locator locator = localBinder.getService();
        locator.setHandler(this.handler);
        this.isBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        this.isBound = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION}, 10);
        } else {
            this.bindService();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(this.isBound) {
            unbindService(this);
        }
    }
}