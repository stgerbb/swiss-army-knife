package ch.ffhs.esa.swissarmyknife.activity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import ch.ffhs.esa.swissarmyknife.R;

/**
 * HeightMeter UI
 * Measures and displays the current height using the pressure sensor.
 *
 * @version 1.0
 */
public class HeightMeterActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_height_meter);
        this.sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        this.textView = findViewById(R.id.txt_height);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float height = this.sensorManager.getAltitude(sensorManager.PRESSURE_STANDARD_ATMOSPHERE, event.values[0]);
        this.textView.setText(Float.toString(height).substring(0, Float.toString(height).indexOf(".") + 2) + " Meter");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Not in use
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(this.sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) != null) {
            this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Snackbar.make(findViewById(R.id.activity_height_meter), R.string.no_pressure_sensor, Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(this.sensorManager != null) {
            this.sensorManager.unregisterListener(this);
        }
    }
}