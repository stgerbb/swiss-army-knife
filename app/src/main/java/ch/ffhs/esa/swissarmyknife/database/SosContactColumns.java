package ch.ffhs.esa.swissarmyknife.database;

/**
 * SosContactColumns Interface
 * Interface for the Columns of the SOS contact table.
 *
 * @version 2.0
 */
public interface SosContactColumns {
    String ID = "_id";
    String NAME = "name";
    String MOBILE_NUMBER = "mobile_number";
}