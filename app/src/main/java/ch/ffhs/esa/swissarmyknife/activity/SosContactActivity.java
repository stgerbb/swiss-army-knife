package ch.ffhs.esa.swissarmyknife.activity;

import android.Manifest;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import ch.ffhs.esa.swissarmyknife.R;
import ch.ffhs.esa.swissarmyknife.database.Database;
import ch.ffhs.esa.swissarmyknife.database.SosContactTbl;

/**
 * SosContact UI
 * Activity to select, add and delete SOS contacts. Uses a database to persistently store the contacts.
 *
 * @version 1.0
 */
public class SosContactActivity extends ListActivity {
    private Database db;
    private SQLiteDatabase dbConn;
    private SimpleCursorAdapter adapter;
    private ListView listView;
    private long itemId;
    private View activity_sos_contact;

    public void onClickAddContact(View v) {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.READ_CONTACTS }, 10);
        } else {
            this.startSubActivity();
        }
    }

    private void startSubActivity() {
        Intent intent = new Intent(this, ContactActivity.class);
        startActivityForResult(intent, 10);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 10) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.startSubActivity();
            }
        }
    }

    public void onClickDeleteContact(View v) {
        if(this.itemId != 0) {
            this.listView.setSelector(android.R.color.transparent);
            this.dbConn.delete(SosContactTbl.TABLE_NAME, " _id = " + this.itemId, null);
            this.adapter.getCursor().requery();
            this.itemId = 0;
            Snackbar.make(this.activity_sos_contact, R.string.contact_deleted, Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(this.activity_sos_contact, R.string.select_contact, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        listView.setSelector(android.R.color.holo_blue_light);
        this.itemId = this.adapter.getItemId(position);
        Snackbar.make(this.activity_sos_contact, R.string.contact_selected, Snackbar.LENGTH_LONG).show();
        this.listView = listView;
        this.saveToSharedPreferences("itemId", this.itemId);
    }

    private void saveToSharedPreferences(String key, long item) {
        SharedPreferences sharedPreferences = getSharedPreferences("Number", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, item);
        editor.apply();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == 10) {
            Bundle bundle = data.getExtras();
            if(bundle != null) {
                String[] array = bundle.getStringArray("contact");
                ContentValues values = new ContentValues();
                values.put(SosContactTbl.NAME, array[0]);
                values.put(SosContactTbl.MOBILE_NUMBER, array[1].replace(" ", ""));
                this.dbConn.insert(SosContactTbl.TABLE_NAME, null, values);
                this.adapter.getCursor().requery();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos_contact);
        this.activity_sos_contact = findViewById(R.id.activity_sos_contact);
        this.setListAdapter();
    }

    private void setListAdapter() {
        this.db = new Database(this);
        this.dbConn = db.getReadableDatabase();
        String[] dbColumns = new String[] {SosContactTbl.ID, SosContactTbl.NAME, SosContactTbl.MOBILE_NUMBER };
        String[] from = new String[] {SosContactTbl.NAME, SosContactTbl.MOBILE_NUMBER };
        int[] to = new int[] {android.R.id.text1, android.R.id.text2 };
        Cursor cursor = this.dbConn.query(SosContactTbl.TABLE_NAME, dbColumns, null, null, null, null, null, null);
        this.adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, from, to, this.adapter.FLAG_REGISTER_CONTENT_OBSERVER);
        setListAdapter(this.adapter);
    }

    /**
     * Used for test purposes.
     *
     * @return adapter
     */
    public SimpleCursorAdapter getSimpleCursorAdapter() {
        return this.adapter;
    }

    /**
     * Used for test purposes.
     *
     * @return itemId
     */
    public long getItemId() {
        return this.itemId;
    }

    /**
     * Used for test purposes.
     *
     * @return itemId
     */
    public SQLiteDatabase getDbConn() {
        return this.dbConn;
    }
}