package  ch.ffhs.esa.swissarmyknife.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import ch.ffhs.esa.swissarmyknife.R;
import ch.ffhs.esa.swissarmyknife.service.Locator;

/**
 * Location UI
 * This Activity shows the map (google maps) with the actual position of the user.
 *
 * @version 1.0
 */
public class LocationActivity extends FragmentActivity implements ServiceConnection, OnMapReadyCallback {
    private Locator locator;
    private GoogleMap googleMap;
    private Marker marker;
    private boolean isBound;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message message) {
            Bundle bundle = message.getData();
            double latitude = bundle.getDouble("latitude");
            double longitude = bundle.getDouble("longitude");
            LatLng currentLocation = new LatLng(latitude, longitude);
            if(marker != null) {
                marker.remove();
                marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).title(getResources().getString(R.string.current_location) + " [" + latitude + ", " + longitude + "]"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
            } else {
                marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).title(getResources().getString(R.string.current_location) + " [" + latitude + ", " + longitude + "]"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
            }
        }
    };

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setTrafficEnabled(true);
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.5f));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        this.isBound = false;
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 10) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.bindService();
            }
        }
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Locator.LocalBinder localBinder = (Locator.LocalBinder) iBinder;
        this.locator = localBinder.getService();
        this.locator.setHandler(this.handler);
        this.isBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {;
        this.isBound = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, 10);
        } else {
            this.bindService();
        }
    }

    private void bindService() {
        Intent intent = new Intent(this, Locator.class);
        bindService(intent, this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(this.isBound) {
            unbindService(this);
        }
    }
}