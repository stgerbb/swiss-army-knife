package ch.ffhs.esa.swissarmyknife.activity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import ch.ffhs.esa.swissarmyknife.R;

/**
 * SpiritLevel UI
 * Activity that shows the Spirit Level of an Object to the user. Uses the Gyroscope Sensor.
 *
 * @version 1.0
 */
public class SpiritLevelActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private TextView txtX;
    private TextView txtY;
    private View activity_spirit_level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spirit_level);
        this.sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.activity_spirit_level = findViewById(R.id.activity_spirit_level);
        this.txtX = findViewById(R.id.txt_x_degree);
        this.txtY = findViewById(R.id.txt_y_degree);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        double x = sensorEvent.values[0] * 9.18;
        double y = sensorEvent.values[1] * 9.35;
        int xCast = (int) x;
        int yCast = (int) y;
        if(xCast < 0) {
            this.txtX.setText(Integer.toString(xCast * -1) + "°");
        } else if(xCast == 90 || xCast == 0) {
            this.txtX.setText(Integer.toString(xCast) + "° [ebene Fläche]");
        } else {
            this.txtX.setText(Integer.toString(xCast) + "°");
        }
        if(yCast < 0) {
            this.txtY.setText(Integer.toString(yCast * -1) + "°");
        } else {
            this.txtY.setText(Integer.toString(yCast) + "°");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // Not in use
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Snackbar.make(this.activity_spirit_level, R.string.no_accelerometer, Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(this.sensorManager != null) {
            this.sensorManager.unregisterListener(this);
        }
    }
}