package ch.ffhs.esa.swissarmyknife.activity;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.ffhs.esa.swissarmyknife.R;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * UI test class for the SosMessageActivity. Explanations of the test cases for each method
 * can be found in the test plan (refer to Test-Identifiers).
 *
 * @version 1.0
 */
@RunWith(AndroidJUnit4.class)
public class SosMessageActivityTest {

    @Rule
    public ActivityTestRule<SosMessageActivity> mActivityRule = new ActivityTestRule<SosMessageActivity>(SosMessageActivity.class);

    @Test
    public void testInput() {
        try {
            Thread.sleep(1000);

            onView(withId(R.id.btn_erase_message)).perform(click());
            onView(withId(R.id.txt_sos_message)).perform(typeText("This is a test message, don't send help..."));
            onView(withId(R.id.txt_sos_message)).check(matches(withText("This is a test message, don't send help...")));
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test-Identifier: FT-2-A
     */
    @Test
    public void testSaveFile() {
        try {
            Thread.sleep(1000);

            onView(withId(R.id.btn_erase_message)).perform(click());
            onView(withId(R.id.txt_sos_message)).perform(typeText("This is a test message, don't send help..."));
            closeSoftKeyboard();
            Thread.sleep(1000);

            onView(withId(R.id.btn_save_message)).perform(click());
            onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.message_saving_ok))).check(matches(isDisplayed()));
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test-Identifier: FT-2-B
     */
    @Test
    public void testSaveFileError() {
        try {
            Thread.sleep(1000);

            onView(withId(R.id.btn_erase_message)).perform(click());
            onView(withId(R.id.txt_sos_message)).perform(typeText("test".trim()));
            closeSoftKeyboard();
            Thread.sleep(1000);

            onView(withId(R.id.btn_save_message)).perform(click());
            onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.message_to_short))).check(matches(isDisplayed()));
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test-Identifier: FT-3-A
     */
    @Test
    public void testDeleteMessage() {
        try {
            Thread.sleep(1000);

            onView(withId(R.id.btn_erase_message)).perform(click());
            Thread.sleep(1000);

            onView(withId(R.id.txt_sos_message)).check(matches(withText("")));
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}