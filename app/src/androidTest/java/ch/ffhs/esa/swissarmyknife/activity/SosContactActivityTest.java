package ch.ffhs.esa.swissarmyknife.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.SimpleCursorAdapter;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.ffhs.esa.swissarmyknife.R;
import ch.ffhs.esa.swissarmyknife.database.SosContactTbl;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.core.AllOf.allOf;

/**
 * UI test class for the SosContactActivity. Explanations of the test cases for each method
 * can be found in the test plan (refer to Test-Identifiers).
 *
 * @version 1.0
 */
@RunWith(AndroidJUnit4.class)
public class SosContactActivityTest {

    @Rule
    public ActivityTestRule<SosContactActivity> mActivityRule = new ActivityTestRule<SosContactActivity>(SosContactActivity.class);

    /**
     * Test-Identifier: FT-1-A
     */
    @Test
    public void testSelectSosContact() {
        try {
            SimpleCursorAdapter adapter = mActivityRule.getActivity().getSimpleCursorAdapter();

            /*
            Tests if the List is empty. If yes, a contact is added to the sos-contact list
             */
            if (adapter.isEmpty()) {
                Thread.sleep(1000);
                onView(withId(R.id.btn_add_contact)).perform(click());

                Thread.sleep(1000);
                onData(anything()).inAdapterView(withId(android.R.id.list)).atPosition(0).perform(click());
            }

            onData(anything()).inAdapterView(withId(android.R.id.list)).atPosition(0).perform(click());
            Thread.sleep(1000);

            onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.contact_selected))).check(matches(isDisplayed()));
            Thread.sleep(1000);

            long itemID = mActivityRule.getActivity().getItemId();
            assertNotNull(itemID);

            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test-Identifier: FT-1-B
     */
    @Test
    public void testEmptyList() {
        try {
            SimpleCursorAdapter adapter = mActivityRule.getActivity().getSimpleCursorAdapter();

            while (!adapter.isEmpty()) {
                onData(anything()).inAdapterView(withId(android.R.id.list)).atPosition(0).perform(click());
                onView(withId(R.id.btn_delete_contact)).perform(click());
                Thread.sleep(1000);
            }

            Thread.sleep(1000);
            onData(anything()).inAdapterView(withId(android.R.id.list)).onChildView(hasChildCount(0));

            Thread.sleep(1000);
            onView(withId(android.R.id.empty)).check(matches(isDisplayed()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test-Identifier: FT-4-A
     */
    @Test
    public void testAddContact() {
        try {
            SimpleCursorAdapter adapter = mActivityRule.getActivity().getSimpleCursorAdapter();

            int countBefore = adapter.getCount();

            Thread.sleep(1000);
            onView(withId(R.id.btn_add_contact)).perform(click());

            Thread.sleep(1000);
            onData(anything()).inAdapterView(withId(android.R.id.list)).atPosition(0).perform(click());

            int countAfter = adapter.getCount();

            Thread.sleep(1000);
            assertTrue(greaterThan(countAfter, countBefore));

            Thread.sleep(1000);
            onData(anything()).inAdapterView(withId(android.R.id.list)).atPosition(adapter.getCount() - 1).perform(click());
            long itemID = mActivityRule.getActivity().getItemId();
            assertTrue(isIDInDB(itemID));

            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test-Identifier: FT-5-A
     */
    @Test
    public void testDeleteContact() {
        try {
            SimpleCursorAdapter adapter = mActivityRule.getActivity().getSimpleCursorAdapter();

            /*
            Tests if the List is empty. If yes, a contact is added to the sos-contact list
             */
            if (adapter.isEmpty()) {
                Thread.sleep(1000);
                onView(withId(R.id.btn_add_contact)).perform(click());

                Thread.sleep(1000);
                onData(anything()).inAdapterView(withId(android.R.id.list)).atPosition(0).perform(click());
            }

            Thread.sleep(1000);

            onData(anything()).inAdapterView(withId(android.R.id.list)).atPosition(0).perform(click());
            Thread.sleep(1000);

            long itemID = mActivityRule.getActivity().getItemId();

            onView(withId(R.id.btn_delete_contact)).perform(click());
            Thread.sleep(1000);

            onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.contact_deleted))).check(matches(isDisplayed()));
            Thread.sleep(1000);

            assertFalse(isIDInDB(itemID));
            Thread.sleep(1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This Method checks if an Contact with a specific ID is in the database.
     *
     * @param id -> id of the contact
     * @return true if contact is in DB, false if contact is not in DB
     */
    private boolean isIDInDB(long id) {
        SQLiteDatabase dbConn = mActivityRule.getActivity().getDbConn();

        String idString = "" + id;
        String[] dbColumns = new String[]{SosContactTbl.ID};
        String[] where = new String[]{idString};

        Cursor cursor = dbConn.query(SosContactTbl.TABLE_NAME, dbColumns, "_id = ?", where, null, null, null);
        int count = cursor.getCount();

        if (count == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method checks if a is greater than b.
     *
     * @param a
     * @param b
     * @return true if a > b, false else
     */
    private boolean greaterThan(int a, int b) {
        return a > b;
    }
}